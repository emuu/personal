import discord
from discord.ext import commands
import card_data
import database


prefix = "!"
bot = commands.Bot(command_prefix=prefix)


@bot.event
async def on_ready():
    database.connect()
    print("Everything's ready to go~")


@bot.event
async def on_message(message):
    print("The message was", message.content)
    await bot.process_commands(message)


@bot.command()
async def ping(ctx):
    latency = bot.latency
    await ctx.send(latency)


@bot.command()
async def deck(ctx, *, content: str):
    decklist = content.split('\n')
    database.submit_decklist(decklist)
    await ctx.send("Decklist submitted")

@bot.command()
async def win(ctx, *, content: str):
    decklist = content.split('\n')
    database.submit_winner(decklist)
    await ctx.send("Winning decklist submitted")


@bot.command()
async def card(ctx, *, content: str):
    await ctx.send(card_data.image(content))
    await ctx.send(database.is_legal(card_data.name(content)))


@bot.command()
async def legal(ctx, *, content: str):
    await None


bot.run('NjEwMzc3ODU3OTk4MjU4MTg3.XVEY0g.GCpyy5_JWGxWS4HU74UhNTLElpw')

